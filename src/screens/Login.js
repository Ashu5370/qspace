
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';
import Firebase from '../config/firebase';
export default class Login extends Component {

  state = {
    email: "",
    password: "",
    errorMessage: null
  }

  handleLogin = () => {
    const { email, password } = this.state

    Firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Main'))
      .catch(error => console.log(error))
  }

  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text })
      return false;
    }
    else {
      this.setState({ email: text })
      console.log("Email is Correct");
    }
  }


  render() {
    return (
      <View style={styles.container}>

        <Image
          style={styles.tinyLogo}
          source={require('../images/signIn.png')}
        />

        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            autoCorrect={false}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.validate(text)}
            value={this.state.email}
          />
        </View>

        <View style={styles.inputView} >
          <TextInput
            secureTextEntry
            autoCorrect={false}
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })} />
        </View>


        <TouchableOpacity style={styles.loginBtn}  onPress={this.handleLogin}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>


        <Text style={{ color: 'black', fontSize: 15 }}>Or connect using </Text>


        <TouchableOpacity style={styles.SocialBtn}>
          <Text style={styles.SocialTextGoogle}>Google</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.forgot}>
          <Text >Forgot Password?</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  tinyLogo: {
    position: "absolute",
    width: 200,
    height: 200,
    top: 10,
    marginRight: 30,
    alignItems: "center",
    justifyContent: "center"
  },
  SocialBtn: {
    position: "absolute",
    top: 550,

    borderWidth: 2,
    marginBottom: 10,
    borderRadius: 20,
    width: "30%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderColor: '#3b5998'

  },
  forgot: {
    position: "absolute",
    marginTop: 20,
    left: 220,
    color: "black",
    fontSize: 13
  },

  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20
  },
  inputView: {
    width: "70%",
    borderBottomWidth: 1.0,
    height: 50,
    marginBottom: 30,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 60,
    color: "black"
  },

  loginBtn: {
    width: "60%",
    backgroundColor: "#00bfa6",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  },
  loginText: {
    color: "white"
  }
});