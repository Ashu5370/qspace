import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation'
import Login from '../screens/Login';
import Home from '../screens/Home';
import SignUpScreen from '../screens/SignUpScreen'
import MainScreen from '../screens/main'
const NavStack = createStackNavigator({
    Home: { 
        screen: Home,
    },
    Login: { 
        screen: Login,
    },
    SignUp: { 
        screen: SignUpScreen,
    },
    Main: {
        screen: MainScreen,
    }
},{
    initialRouteName: 'Home'
  });

const Navigation = createAppContainer(NavStack);

export default Navigation;