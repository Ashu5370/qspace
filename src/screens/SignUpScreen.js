
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';
import Firebase from '../config/firebase';

export default class SignUpScreen extends Component {

  state = {
    email: "",
    password: "",
    errorMessage: null,
    c_password:'',
    MNumber:'',
  }


  handleSignUp = () => {
    const { email, password } = this.state
    Firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Login'))
      .catch(error => console.log(error))
  }

  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text })
      return false;
    }
    else {
      this.setState({ email: text })
      console.log("Email is Correct");
    }
  }

  


  render() {
    return (
      <View style={styles.container}>

      <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            autoCorrect={false}
            placeholder="Mobile Number"
            placeholderTextColor="#003f5c"
            onChangeText={text =>  this.setState({ MNumber : text })}
            value={this.state.MNumber}
          />
        </View>

        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            autoCorrect={false}
            placeholder="Email"
            placeholderTextColor="#003f5c"
            onChangeText={text => this.validate(text)}
            value={this.state.email}
          />
        </View>

        <View style={styles.inputView} >
          <TextInput
            secureTextEntry
            autoCorrect={false}
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#003f5c"
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })} />
        </View>


        

        <TouchableOpacity style={styles.SocialBtn} onPress={this.handleSignUp}>
          <Text style={styles.SocialTextGoogle}>SIGN UP</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  SocialBtn: {
    position: "absolute",
    top: 550,
    borderWidth: 2,
    marginBottom: 10,
    borderRadius: 20,
    width: "60%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderColor: '#3b5998',
    backgroundColor: "#00bfa6"

  },
  SocialTextFB: {
    color: "#3b5998"

  },
  SocialTextGoogle: {

  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20
  },
  inputView: {
    width: "70%",
    borderBottomWidth: 1.0,
    height: 50,
    marginBottom: 30,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 60,
    color: "black"

  },
  forgot: {
    marginTop: 20,
    color: "black",
    fontSize: 13
  },
  loginBtn: {
    width: "60%",
    backgroundColor: "#00bfa6",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  },
  loginText: {
    color: "white"
  }
});