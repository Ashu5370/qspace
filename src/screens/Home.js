import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';

export default class Home extends Component {

    render() {
        return (
            <View style={styles.container}>


             {/*    Sample LOGO */}
                <Image
                    style={styles.tinyLogo}
                    source={require('../images/logo.png')}
                />

                <TouchableOpacity style={styles.SignUpBtn} onPress={() => this.props.navigation.navigate('SignUp')}>
                    <Text style={styles.SocialTextGoogle}>SIGN UP</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.LoginBtn} onPress={() => this.props.navigation.navigate('Login')} >
                    <Text style={styles.SocialTextGoogle}>LOGIN</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    LoginBtn: {
        position: "absolute",
        top: 630,
        borderWidth: 2,
        borderRadius: 20,
        width: "65%",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        borderColor: '#3b5998',
        backgroundColor: "#00bfa6",
    },

    SignUpBtn: {
        position: "absolute",
        top: 550,
        borderWidth: 2,
        borderRadius: 20,
        width: "65%",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        borderColor: '#3b5998'
    },


    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    tinyLogo: {
        position: "absolute",
        width: 300,
        height: 400,
        top: 40,
        marginRight: 30,
        alignItems: "center",
        justifyContent: "center"
    }



});